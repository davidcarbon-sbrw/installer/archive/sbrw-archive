;aiu;

[1.9.94]
Name = SBRW Launcher Update
ProductVersion = 1.9.94.0
URL = http://1davidcarbon.gitlab.io/soapbox-installer-releases-download/2-0-7-4/SBRW Installer.exe
URL1 = https://l-sbrw.davidcarbon.download/2-0-7-4/SBRW Installer.exe
Size = 12634840
ReleaseDate = 27/12/2019
SHA256 = 3A434A502D1F0822D7BC903DC771B0F1668CD58A841BA94A7E082DB46F2C701E
MD5 = e57f0b800cbbccf2750fd27fd150db96
ServerFileName = SBRW Installer.exe
Flags = NoCache
RegistryKey = HKUD\Software\Soapbox Race World\Soapbox Race World Launcher\Version
Version = 1.9.94.0
UpdatedApplications = Soapbox Race World Launcher[1.9.40-1.9.93]
Description = <i> This update affects the Game Launcher and Launcher Installer. </i>
Description1 = <i> You can ingnore this update if you have already successfully updated your Game Launcher. </i>
BugFix = <b> Game Launcher Updates </b>
BugFix1 = Fix for ModNetReloaded, because i forgot ElectronModNet uses almost same technique to update files
BugFix2 = Fix for most of the Out of Memory issues caused by Launcher Proxy
Replaces = All
